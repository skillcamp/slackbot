# Skillcamp Bot

The Skillcamp slack bot.

## Getting Started

Install the latest version of go (requires version 1.11.0 or greater) through
your package manager or via the [official instructions][go-install]

Clone and run it locally:

    git clone https://gitlab.com/skillcamp/slackbot.git
    cd slackbot
    go run .

_Note: We are using [go modules] (as part of go 1.11.0) so you do not need to
setup your GOPATH or have to clone it into your GOPATH (though you still can)._

[go-install]: https://golang.org/doc/install
[go modules]: https://github.com/golang/go/wiki/Modules
